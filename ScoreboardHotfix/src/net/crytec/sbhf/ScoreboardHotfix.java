package net.crytec.sbhf;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Team;

public class ScoreboardHotfix extends JavaPlugin implements CommandExecutor {

	
	
	@Override
	public void onEnable() {
		
		if (!getDataFolder().exists()) {
			getDataFolder().mkdir();
		}

		File config = new File(getDataFolder(), "config.yml");

		try {
			if (!config.exists()) {
				config.createNewFile();
				saveResource("config.yml", true);
				log("Setup - New default configuration has been written.");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		log("�ePurged " + this.purge() + " empty teams!");
		
		if (getConfig().getInt("AutoPurgeMin") > 0) {
		log("Purging scoreboard every �e" + getConfig().getInt("AutoPurgeMin") + " minutes!");
		Bukkit.getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
			@Override
			public void run() {
				log("�ePurged " + purge() + " empty teams!");
			}

		}, 20 * 60 * getConfig().getInt("AutoPurgeMin"), 20 * 60 * getConfig().getInt("AutoPurgeMin"));
	  }
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		if (!sender.hasPermission("sbhf.run")) {
			sender.sendMessage("�3You lack the proper permission to use this command!");
			return true;
		}
		sender.sendMessage("�ePurged �d" + purge() + "�e empty teams!");
		return true;
		
	}
	
	
	public int purge() {
		int i = 0;
		for (Team team : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()) {
			if (team.getEntries().isEmpty()) {
				team.unregister();
				i++;
			}
		}
		return i;
	}
	
	
	public void log(String message) {
		Bukkit.getConsoleSender().sendMessage("[ScoreboardHotfix] " + message);
	}
	
}
